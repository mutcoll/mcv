/**
 * @file Physics.cpp
 * @author jmmut
 * @date 2016-08-27.
 */

#include <log/log.h>
#include <sstream>
#include <utils/exception/StackTracedException.h>
#include "Physics.h"

const double SMALL_FLOAT = 0.0001;


bool canInteract(const ParticleCreation &firstParticle, const ParticleCreation &secondParticle) {
    double INTERACTION_DISTANCE = 0.5;
    return computeDistance(firstParticle, secondParticle) < INTERACTION_DISTANCE;
}

bool hasConeDefined() {
    return true;    // XD next version will be random
}

bool Physics::interact(std::shared_ptr<Digraph<Interaction>::Node> firstNode, ParticleCreation &firstParticle,
        std::shared_ptr<Digraph<Interaction>::Node> secondNode, ParticleCreation &secondParticle) {
    if (canInteract(firstParticle, secondParticle)) {
        if (hasConeDefined()) {
            const Particle &interactionParticle = computeParticleInteraction(firstParticle, secondParticle);

            // optional: remove particles from old nodes
            firstNode->node.createdUndefinedParticles.erase(
                    std::find(firstNode->node.createdUndefinedParticles.begin(),
                            firstNode->node.createdUndefinedParticles.end(), firstParticle));
            secondNode->node.createdUndefinedParticles.erase(
                    std::find(secondNode->node.createdUndefinedParticles.begin(),
                            secondNode->node.createdUndefinedParticles.end(), secondParticle));

            // write them into the new interaction
            Particle firstDestroyedParticle(firstParticle.creationPoint, interactionParticle.creation);
            Particle secondDestroyedParticle(secondParticle.creationPoint, interactionParticle.destruction);
            ParticleCreation firstTransformed(firstDestroyedParticle.destruction,
                    VectoredLine(secondDestroyedParticle).direction.Unit());
            ParticleCreation secondTransformed(secondDestroyedParticle.destruction,
                    VectoredLine(firstDestroyedParticle).direction.Unit());
            const std::shared_ptr<Digraph<Interaction>::Node> &newNode = digraph.addNode(Interaction({
                    firstDestroyedParticle, secondDestroyedParticle, interactionParticle}, {
                    firstTransformed, secondTransformed}, {}));

            // add connections to this new node
            digraph.addConnection(firstNode, newNode);
            digraph.addConnection(secondNode, newNode);
            return true;
        }
    }
    return false;
}

std::string to_string(Vector3D v) {
    std::stringstream ss;
    ss << v;
    return ss.str();
}

/**
 * pseudoalgorithm:
 *  foreach combination of two particleCreation
 *      find destruction point: dp
 *      if dp has all its past-cone defined
 *          dp can be created as a new Interaction
 *          both particles move from undefinedCreated to defineddestroyed in the new Interaction
 *          add a node into the graph
 *          add two connections to the new node
 *          break loop
 */
void Physics::advance() {

    using Node = Digraph<Interaction>::Node;
    std::shared_ptr<Node> minFirstNode, minSecondNode;
    std::vector<ParticleCreation>::iterator minFirstParticle;
    std::vector<ParticleCreation>::iterator minSecondParticle;
    double minDistance, candidateMinDistance;
    triangularLoop(digraph.getLeafs(),
            [&](
                    std::set<std::shared_ptr<Node>>::iterator firstNode,
                    std::set<std::shared_ptr<Node>>::iterator secondNode) {

                auto func = [&](std::vector<ParticleCreation>::iterator firstParticle,
                        std::vector<ParticleCreation>::iterator secondParticle) {

                    candidateMinDistance = std::abs(
                            computeDistance(*firstParticle, *secondParticle));
                    if (candidateMinDistance < minDistance || minFirstNode == nullptr) {
                        minDistance = candidateMinDistance;
                        minFirstNode = *firstNode;
                        minSecondNode = *secondNode;
                        minFirstParticle = firstParticle;
                        minSecondParticle = secondParticle;
                    }
                    return true;
                };

                // combinations inside node
                if (not triangularLoop((*firstNode)->node.createdUndefinedParticles, func)) {
                    return false;
                }

                // combinations across nodes
                std::vector<ParticleCreation> &firstNodeParts = (*firstNode)->node.createdUndefinedParticles;
                std::vector<ParticleCreation> &secondNodeParts = (*secondNode)->node.createdUndefinedParticles;
                for (auto particleIt = firstNodeParts.begin(); particleIt != firstNodeParts.end(); particleIt++) {
                    for (auto secondIt = secondNodeParts.begin(); secondIt != secondNodeParts.end(); secondIt++) {
                        candidateMinDistance = std::abs(computeDistance(*particleIt, *secondIt));
                        if (minFirstNode == nullptr || candidateMinDistance < minDistance) {
                            minDistance = candidateMinDistance;
                            minFirstNode = *firstNode;
                            minSecondNode = *secondNode;
                            minFirstParticle = particleIt;
                            minSecondParticle = secondIt;
                        }
                    }
                }
                return true;
            }
    );

    if (minFirstNode != nullptr) {
        LOG_DEBUG("min distance: %f", minDistance);
        LOG_EXP(to_string(minFirstParticle->creationPoint).c_str(), "%s");
        LOG_EXP(to_string(minSecondParticle->creationPoint).c_str(), "%s");

        interact(minFirstNode, *minFirstParticle, minSecondNode, *minSecondParticle);
    } else {
        LOG_DEBUG("no min distance (%f)", minDistance);
    }
}




Particle computeParticleInteraction(ParticleCreation first, ParticleCreation second) {
    Vector3D point = computeNearestPoint(first, second);
    Vector3D normal = first.direction ^ second.direction;
    double distance = computeDistance(first, second);
    return Particle(point, point + normal.Unit() * distance);
}


double computeDistance(const randomize::math::Line &first, const randomize::math::Line &second) {
    return computeDistance(first.P0, first.P1 - first.P0, second.P0, second.P1 - second.P0);
}
double computeDistance(const VectoredLine &first, const VectoredLine &second) {
    return computeDistance(first.P0, first.direction, second.P0, second.direction);
}

/**
 * The normal is the line perpendicular to both lines, which is the normal of the plane
 * that the direction vectors of both lines define.
 * Then, take any two points belonging to each line, and do the projection onto the normal using
 * dot product:  n*b = |n| * |b| * cos (alfa_n_b); n*b / |n| = |b| * cos(alfa_n_b);
 * n is the normal
 * b is the line from a point in the first line to a point in the second line
 * |b| * cos(alfa_n_b) is the distance from one line to the other
 */
double computeDistance(const Vector3D &pointFirst, const Vector3D &directionFirst,
        const Vector3D &pointSecond, const Vector3D &directionSecond) {
    const Vector3D &normal = directionFirst^directionSecond;
    if (normal.Modulo() < SMALL_FLOAT) {
        return (pointSecond - pointFirst).Modulo();
    } else {
        return ((pointSecond - pointFirst) * normal) / normal.Modulo();
    }
}



Vector3D computeNearestPoint(const randomize::math::Line &first,
        const randomize::math::Line &second) {
    return computeNearestPoint(first.P0, first.P1 - first.P0, second.P0, second.P1 - second.P0);
}
Vector3D computeNearestPoint(const VectoredLine &first, const VectoredLine &second) {
    return computeNearestPoint(first.P0, first.direction, second.P0, second.direction);
}

Vector3D computeNearestPoint(const Vector3D &pointFirst, const Vector3D &directionFirst,
        const Vector3D &pointSecond, const Vector3D &directionSecond) {
    const Vector3D c = pointSecond - pointFirst;
    const Vector3D normal = directionFirst^directionSecond;
    if (normal.Modulo() < SMALL_FLOAT) {
        throw randomize::utils::exception::StackTracedException("There's no single point of minimum distance");
    } else {
        return pointFirst + directionFirst * (((c ^ directionSecond) * normal) / (normal * normal));
    }
}







