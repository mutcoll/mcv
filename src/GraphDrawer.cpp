/**
 * @file GraphDrawer.cpp
 * @author jmmut
 * @date 2016-08-27.
 */

#include <GL/gl.h>
#include "GraphDrawer.h"
#include "SphereLOD.h"


void GraphDrawer::draw(Digraph<Interaction> &digraph) {
    for(std::shared_ptr<Digraph<Interaction>::Node> node : digraph.getRoots()) {
        drawNode(*node);
    }

}

void GraphDrawer::drawNode(Digraph<Interaction>::Node &node) {
    for (Particle &particle : node.node.destroyedParticles) {
        ParticleDrawer().draw(particle);
    }

    for (ParticleCreation &particle : node.node.createdUndefinedParticles) {
        ParticleCreationDrawer().draw(particle);
    }
    for (auto successor : node.successors) {
        drawNode(*successor);
    }
}


void ParticleDrawer::draw(Particle &particle) {
    glBegin(GL_TRIANGLES);
    SphereLOD(particle.creation + (particle.destruction - particle.creation)*0.25, 0.1, {{0.3, 0.3, 1}}).draw(1);
    SphereLOD(particle.destruction - (particle.destruction - particle.creation)*0.25, 0.1, {{0.3, 0.3, 1}}).draw(1);
    glEnd();
    glBegin(GL_LINES);
    glColor3f(0.3, 0.3, 1);
    glVertex3fv(particle.creation.v);
    glVertex3fv(particle.destruction.v);
    glEnd();
}


void ParticleCreationDrawer::draw(ParticleCreation &particle) {
    glBegin(GL_TRIANGLES);
    SphereLOD(particle.creationPoint + particle.direction*0.25, 0.1, {{1, 0.3, 0.3}}).draw(1);
    glEnd();
    glBegin(GL_LINES);
    glColor3f(1, 0.3, 0.3);
    glVertex3fv(particle.creationPoint.v);
    glVertex3fv((particle.creationPoint + particle.direction).v);
    glEnd();
}

