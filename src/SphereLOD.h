/**
 * @file SphereLOD.h
 * @author jmmut
 * @date 2016-04-13.
 */

#ifndef GRAVITYTAMER_SPHERELOD_H
#define GRAVITYTAMER_SPHERELOD_H

#include <algorithm>
#include <array>
#include "math/Vector3D.h"

class SphereLOD {
public:

    SphereLOD(const Vector3D &center, double radius)
            : center(center), radius(radius), distribution(0.5, 1.0), random_color(true) {
    }
    SphereLOD(const Vector3D &center, double radius, std::array<float, 3> color)
            : center(center), radius(radius), distribution(0.5, 1.0),
            random_color(false), color(color) {
    }

    void draw(double polygonOrderOfMagnitude);
    Vector3D & getPos();
    double & getRadius();

private:
    Vector3D center;
    double radius;
    std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution;
    // distance in a cube from center to an edge if the distance to a vertex is 1
    float apothem;

    bool random_color;
    std::array<float, 3> color;

    void drawFace(int detail, std::array<Vector3D, 3> faceVertices);

    Vector3D middleVertex(Vector3D u, Vector3D v);
};


#endif //GRAVITYTAMER_SPHERELOD_H
