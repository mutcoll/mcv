/**
 * @file ParticleCreation.h
 * @author jmmut
 * @date 2016-08-27.
 */

#ifndef MCV_PARTICLECREATION_H
#define MCV_PARTICLECREATION_H


#include <math/Vector3D.h>
#include <math/Geom.h>

struct VectoredLine {
    Vector3D P0;
    Vector3D direction;
};

class ParticleCreation {
public:

    ParticleCreation(const Vector3D &creationPoint, const Vector3D &direction)
            : creationPoint(creationPoint), direction(direction) { }

    explicit operator randomize::math::Line() const { return randomize::math::Line{creationPoint, creationPoint + direction}; }
    operator VectoredLine() const { return VectoredLine{creationPoint, direction}; }
    bool operator==(ParticleCreation other) {
        return creationPoint == other.creationPoint && direction.Unit() == other.direction.Unit();
    }
//    operator randomize::math::Ray() { return randomize::math::Ray{creationPoint, direction}; }
//private:
    Vector3D creationPoint;
    Vector3D direction;
};


#endif //MCV_PARTICLECREATION_H
