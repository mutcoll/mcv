/**
 * @file GraphDrawer.h
 * @author jmmut
 * @date 2016-08-27.
 */

#ifndef MCV_GRAPHDRAWER_H
#define MCV_GRAPHDRAWER_H


#include "Digraph.h"
#include "Interaction.h"

class GraphDrawer {
public:
    void draw(Digraph<Interaction> &digraph);
    void drawNode(Digraph<Interaction>::Node &node);
};

class ParticleDrawer {
public:
    void draw(Particle &particle);
};

class ParticleCreationDrawer {
public:
    void draw(ParticleCreation &particle);
};


#endif //MCV_GRAPHDRAWER_H
