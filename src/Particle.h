/**
 * @file Particle.h
 * @author jmmut
 * @date 2016-08-27.
 */

#ifndef MYTEMPLATEPROJECT_PARTICLE_H
#define MYTEMPLATEPROJECT_PARTICLE_H


#include <math/Vector3D.h>
#include "ParticleCreation.h"

class Particle {

public:

    Particle(const ParticleCreation &particle, const Vector3D &destruction)
            : creation(particle.creationPoint), destruction(destruction) { }

    Particle(const Vector3D &creation, const Vector3D &destruction) : creation(creation),
            destruction(destruction) { }

    explicit operator randomize::math::Line() const { return randomize::math::Line{creation, destruction}; }
    operator VectoredLine() const { return VectoredLine{creation, destruction - creation}; }

//private:
    Vector3D creation;
    Vector3D destruction;
};


#endif //MYTEMPLATEPROJECT_PARTICLE_H
