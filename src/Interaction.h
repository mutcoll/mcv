/**
 * @file Interaction.h
 * @author jmmut
 * @date 2016-08-27.
 */

#ifndef MCV_INTERACTION_H
#define MCV_INTERACTION_H


#include <memory>
#include <vector>
#include "Particle.h"
#include "ParticleCreation.h"

class Interaction {
public:

    Interaction(const std::vector<Particle> &destroyedParticles,
            const std::vector<ParticleCreation> &createdUndefinedParticles,
            const std::vector<Particle> &createdDefinedParticles) : destroyedParticles(
            destroyedParticles), createdUndefinedParticles(createdUndefinedParticles),
            createdDefinedParticles(createdDefinedParticles) { }

private:
public:
    std::vector<Particle> destroyedParticles;
    std::vector<ParticleCreation> createdUndefinedParticles;
    std::vector<Particle> createdDefinedParticles;

};


#endif //MCV_INTERACTION_H
