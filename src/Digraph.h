/**
 * @file Digraph.h
 * @author jmmut
 * @date 2016-08-27.
 */

#ifndef MCV_DIGRAPH_H
#define MCV_DIGRAPH_H


#include <memory>
#include <vector>
#include <set>

template<typename NODE>
class Digraph {
public:
    struct Node {

        Node(NODE node,
                const std::vector<std::shared_ptr<Node>> &predecessors,
                const std::vector<std::shared_ptr<Node>> &successors)
                : node(node), predecessors(predecessors), successors(successors) { }

        NODE node;
        std::vector<std::shared_ptr<Node>> predecessors;
        std::vector<std::shared_ptr<Node>> successors;
    };

    std::shared_ptr<Node> addNode(NODE node) {
//        Node node_temp(node, {}, {});
        std::shared_ptr<Node> new_node = std::make_shared<Node>(Node(node, {}, {}));
        roots.insert(new_node);
        leafs.insert(new_node);
        return new_node;
    }

    void addConnection(std::shared_ptr<Node> source, std::shared_ptr<Node> sink) {
        source->successors.push_back(sink);
        leafs.erase(source);
        sink->predecessors.push_back(source);
        roots.erase(sink);
    }

    const std::set<std::shared_ptr<Node>> &getRoots() const {
        return roots;
    }

    const std::set<std::shared_ptr<Node>> &getLeafs() const {
        return leafs;
    }

private:
    std::set<std::shared_ptr<Node>> roots;
    std::set<std::shared_ptr<Node>> leafs;
};


#endif //MCV_DIGRAPH_H
