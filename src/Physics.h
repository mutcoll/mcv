/**
 * @file Physics.h
 * @author jmmut
 * @date 2016-08-27.
 */

#ifndef MCV_PHYSICS_H
#define MCV_PHYSICS_H


#include <algorithm>
#include <math/Geom.h>
#include "Digraph.h"
#include "Interaction.h"

class Physics {
public:
    Physics(Digraph<Interaction> &digraph) : digraph(digraph) { }

    void advance();

private:
    Digraph<Interaction> &digraph;

    bool interact(std::shared_ptr<Digraph<Interaction>::Node> firstNode, ParticleCreation &firstParticle,
            std::shared_ptr<Digraph<Interaction>::Node> secondNode, ParticleCreation &secondParticle);
};

Particle computeParticleInteraction(ParticleCreation first, ParticleCreation second);

double computeDistance(const randomize::math::Line &first, const randomize::math::Line &second);
double computeDistance(const VectoredLine &first, const VectoredLine &second);
double computeDistance(const Vector3D &pointFirst, const Vector3D &directionFirst,
        const Vector3D &pointSecond, const Vector3D &directionSecond);

// TODO: a version that takes into account the beginning of a particle, and doesn't intersect before it exists
Vector3D computeNearestPoint(const randomize::math::Line &first,
        const randomize::math::Line &second);
Vector3D computeNearestPoint(const VectoredLine &first, const VectoredLine &second);
Vector3D computeNearestPoint(const Vector3D &pointFirst, const Vector3D &directionFirst,
        const Vector3D &pointSecond, const Vector3D &directionSecond);


/**
 * walks the collection and calls the user function will all the pair combinations. example:
 * collection: [a,b]
 * combinations: [[a,b]]
 * collection: [a,b,c]
 * combinations: [[a,b],[a,c],[b,c]]
 * returns what the userFunction returns, that means whether the loop finishes normally.
 */
template<typename T>
bool triangularLoop(T collection,
        std::function<bool(typename T::iterator, typename T::iterator)> userFunction) {
    if (collection.size() >= 2) {
        for (auto firstIt = collection.begin(); firstIt != collection.end(); ++firstIt) {
            auto secondIt = firstIt;
            ++secondIt;
            for (; secondIt != collection.end(); ++secondIt) {
                if (not userFunction(firstIt, secondIt)) {
                    return false;
                }
            }
        }
    }
    return true;
}

#endif //MCV_PHYSICS_H
