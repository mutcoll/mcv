//
// Created by jmmut on 2015-06-09.
//

#ifndef PLOTTER_GESTOR_H
#define PLOTTER_GESTOR_H


#include <stdexcept>
#include "log/log.h"
#include "vgm/Ventana.h"
#include "vgm/ShaderManager.h"
#include "graphics/drawables/Volador.h"
#include "Particle.h"
#include "Digraph.h"
#include "Interaction.h"
#include "Physics.h"


class Gestor: public Ventana
{
public:
    Gestor(int width = 640, int height = 480);
    void initGL() override;
    void onStep(float) override;
    void onDisplay() override;
    void onKeyPressed(SDL_Event &e) override;
    void onPressed(const Pulsado &p) override;
    void onMouseMotion(SDL_Event &e) override;
    void onMouseButton(SDL_Event &e) override;
    
    string help();

private:
    void drawAxes();

    int t;
    float drot, dtrl, mouse_drot, mouse_dtrl;   // delta rotation, delta translation


    Volador vldr;
    Vector3D center;
    bool leftClickPressed;
    bool rightClickPressed;
    Sint32 lastClickX;
    Sint32 lastClickY;

    Digraph<Interaction> causalityGraph;
    Physics physics;
};

#endif //PLOTTER_GESTOR_H
