/**
 * @file SphereLOD.cpp
 * @author jmmut
 * @date 2016-04-13.
 */

#include <GL/gl.h>
#include "SphereLOD.h"

void SphereLOD::draw(double polygonOrderOfMagnitude) {
    const double MAX_POLYGON_ORDER = 20;
    polygonOrderOfMagnitude = std::min(MAX_POLYGON_ORDER, polygonOrderOfMagnitude);

    apothem = std::sqrt(radius * radius / 3.0);

    // fitting a tetrahedron vertices within a cube
    std::vector<Vector3D> vertices = {
            {center.x + apothem, center.y - apothem, center.z + apothem},
            {center.x + apothem, center.y + apothem, center.z - apothem},
            {center.x - apothem, center.y + apothem, center.z + apothem},
            {center.x - apothem, center.y - apothem, center.z - apothem},
    };

    glBegin(GL_TRIANGLES);
    // 4 take 3. combinating the vertices to form the tetrahedron faces
    drawFace(polygonOrderOfMagnitude / 2, {vertices[0], vertices[1], vertices[2]});
    drawFace(polygonOrderOfMagnitude / 2, {vertices[0], vertices[3], vertices[1]});
    drawFace(polygonOrderOfMagnitude / 2, {vertices[0], vertices[2], vertices[3]});
    drawFace(polygonOrderOfMagnitude / 2, {vertices[1], vertices[3], vertices[2]});
    glEnd();
}

Vector3D & SphereLOD::getPos() {
    return center;
}

double &SphereLOD::getRadius() {
    return radius;
}

/**
 * requires an environment of glBegin(GL_TRIANGLES) to be set
 */
void SphereLOD::drawFace(int detail, std::array<Vector3D, 3> faceVertices) {
    if (detail <= 0) {
        for (auto vertex : faceVertices) {
//            glColor3f(distribution(generator), 0.5, 0.5);
            if (random_color) {
                glColor3f(distribution(generator), distribution(generator), distribution(generator));
            } else {
                glColor3fv(color.begin());
            }
            glVertex3fv(vertex.v);
        }
    } else {
        detail--;
        std::vector<Vector3D> middleVertices;
        for (unsigned int i = 0; i < 3; ++i) {
            middleVertices.push_back(middleVertex(faceVertices[i], faceVertices[(i +1) %3]));
        }
        drawFace(detail, {faceVertices[0], middleVertices[2], middleVertices[0]});
        drawFace(detail, {faceVertices[1], middleVertices[0], middleVertices[1]});
        drawFace(detail, {faceVertices[2], middleVertices[1], middleVertices[2]});
        drawFace(detail, {middleVertices[0], middleVertices[1], middleVertices[2]});
    }
}


Vector3D SphereLOD::middleVertex(Vector3D u, Vector3D v) {
    return ((u + v)/2 - center).Unit() * radius + center;
}
